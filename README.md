# pcaplib-test

This project is an example to demonstrate an error in the Linux kernel. It informs that certains packets received of a protocol not known will be marked as dropped, when in reallity it was processed. The program uses the pcaplib to count every packet received in an virtual ethernet interface, linked with another one that it is used to inject this packets.

# Requirements
* pcaplib
* tcpreplay
```console
sudo apt-get install libpcap-dev tcpreplay
```

# run
Execute the script `run.sh`, passing as arguments how many repetitions of the file `singlex7262.pcap` will be injected to the interface and the mode of execution. If the value `auto` is passed, instead of using the tool `tcpreplay` it will use the compiled program to inject packets. Optionally, a third argument indicates the pcap file to inject packets.

This will create two virtual ethernet interfaces (`veth1` and `veth2`), compile the source and execute it to sniff the received packets in the `veth2` interface. Depending if the second argument is passed or not, the injection of the packets will be done with the tool `tcpreplay` or another instance of the compiled program.