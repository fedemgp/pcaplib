#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "pcap_test.h"

// global variable to count the calls to the callback of packet_received
static unsigned int amount_calls_to_callback = 0;
pcap_t *dev = NULL;

bool inject_packets (const char * argv[], pcap_t *dev) {
	char msg_err_buffer[PCAP_ERRBUF_SIZE];
	char *ptr = NULL;
	int amount = strtoul(argv[3], &ptr, 10);

	printf("Starting to send %i packets\n", amount);
	while (amount > 0) {
		// open pcap file to send data
		pcap_t * file = pcap_open_offline(argv[4], msg_err_buffer);	
		if (file == NULL) {
			fprintf(stderr, "%s\n", msg_err_buffer);
			return false;
		}
		// this ends when it reaches end of file
		printf("%i packets left.\n", amount);
		pcap_loop(file, amount, send_packet, NULL);
		amount -= amount_calls_to_callback;
		amount_calls_to_callback = 0;
		pcap_close(file);
	}
	print_stats(dev);
	return true;
}

void send_packet (u_char *args, const struct pcap_pkthdr *header, 
	const u_char *packet) {
	amount_calls_to_callback++;
	if (pcap_inject(dev, packet, header->len) ==  PCAP_ERROR) {
		fprintf(stderr, "Error sending a packet\n");
	}
}

pcap_t * startup_device (const char *dev_str) {
	char msg_err_buffer[PCAP_ERRBUF_SIZE];
	printf("Setting up interface %s.\n", dev_str);
	pcap_t *device = pcap_create(dev_str, msg_err_buffer);
	if (device == NULL) {
		fprintf(stderr, "%s\n", msg_err_buffer);
		return NULL;
	}
	if (!configure_device(device, dev_str)) {
		return NULL;
	}
	// if return value es greater than 0 its a warning, if its negative 
	// is an error.
	int status = pcap_activate(device);
	if (status < 0) {
		pcap_perror(device, "Error when trying to activate the device: ");
		pcap_close(device);
		return NULL;
	} else if (status > 0) {
		pcap_perror(device, "Warning when trying to activate the device: ");
	}
	// checks if device support ethernet headers
	if (pcap_datalink(device) != DLT_EN10MB) {
		fprintf(stderr, "Device %s doesn't provide Ethernet headers -  "
			"not supported\n", dev_str);
		pcap_close(device);
		return NULL;
	}

	return device;
}

bool analyze (pcap_t *device, char const *amount_str) {
	char *ptr;
	int amount = strtoul(amount_str, &ptr, 10);
	// Grab _amount_ packets
	pcap_loop(device, amount, packet_received, NULL);

	print_stats(device);
	return true;
}

void print_stats (pcap_t * device) {
	struct pcap_stat ps;
	pcap_stats(device, &ps);
	printf("|ps_recv\t|ps_drop\t|ps_ifdrop\t|packets processed\t|\n");
	printf("-------------------------------------------------------------------------\n");
	printf("|%i\t\t|%i\t\t|%i\t\t|%i\t\t\t|\n",ps.ps_recv, ps.ps_drop, ps.ps_ifdrop, amount_calls_to_callback );
	printf("\n\n");
}

void packet_received (u_char *args, const struct pcap_pkthdr *header, 
	const u_char *packet) {
	amount_calls_to_callback++;
}

bool configure_device (pcap_t *device, const char *dev_str) {
	// sets timeout to 10 sec
	if (pcap_set_timeout(device, TIMEOUT_MS) != 0) {
		fprintf(stderr, "Couldn't set timeout to %i miliseconds. Device %s was "
			"previously activated.\n", TIMEOUT_MS, dev_str);
		return false;
	}

	if (pcap_set_promisc(device, 1) != 0) {
		fprintf(stderr, "Couldn't set device %s to promiscuous mode. %s was "
			"previously activated.\n", dev_str, dev_str);
		return false;
	}
	
	return true;
}